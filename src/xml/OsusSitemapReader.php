<?php

class OsusSitemapReader {
  
  const FROM_FILE = 1;
  const FROM_STRING = 2;
  
  /**
   * liest eine sitemap aus einem string und liefert einen ArrayIterator mit Urls zurück
   * 
   * @param string $string
   * @returns ArrayIterator
   * @throws Exception on xml error
   */
  public function readLocationsFromString($string) {
    return $this->process(self::FROM_STRING, $string);
  }
  
  
  
  /**
   * liest eine sitemap von einem file oder einer url und liefert einen ArrayIterator mit Urls zurück
   * 
   * @param string $file file oder url
   * @returns ArrayIterator
   * @throws Exception on xml error or file access problems
   */
  public function readLocationsFromFile($file) {
    return $this->process(self::FROM_FILE, $file);
  }
  

  
  private function process($from, $source) {
    libxml_use_internal_errors(true);
    
    switch ($from) {
      case self::FROM_STRING:
        $xml = simplexml_load_string($source);
        break;
      case self::FROM_FILE:
        $xml = simplexml_load_file($source);
        break;
        
    }
    
    $this->handleXmlErrors();
    libxml_clear_errors();
    libxml_use_internal_errors(false);

    return $this->readLocationsFromSimpleXml($xml);
  }
  
  private function handleXmlErrors() {
    if ($error = libxml_get_last_error()) {
      libxml_clear_errors();
      libxml_use_internal_errors(false);
      throw new Exception($error->message);
    }
  }

  
  
  private function readLocationsFromSimpleXml(SimpleXMLElement $sitemapxml) {
    $items = new ArrayIterator();
    foreach($sitemapxml->url as $entry)
    {
       $items->append($entry->loc->__toString());
    }
    
    return $items;
  }
}
