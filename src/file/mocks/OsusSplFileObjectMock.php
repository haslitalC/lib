<?php

class OsusSplFileObjectMock extends SplFileObject {
  private $lastString = "";
  
  public function __construct($filename) {
    parent::__construct($filename);
    $this->setFileClass("OsusSplFileObjectMock");
  }
  
  public function openFile($open_mode = "r", $use_include_path = false, $context = NULL) {
    return $this;
  }
  
  public function fwrite($string, $length = null) {
    $this->lastString = $string;
  }
  
  public function getLastWritten() {
    return $this->lastString;
  }
}
