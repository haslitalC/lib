<?php

class OsusLogger implements OsusLoggerInterface {

  public function info($message) {
    $message = $this->renderMessage("", $message);
    $this->write($message);
  }
  
  public function warning($message) {
    $message = $this->renderMessage("Warning: ", $message);
    $this->write($message);
  }
  
  public function error($message) {
    $message = $this->renderMessage("ERROR: ", $message);
    $this->write($message);
  }
  
  protected function renderMessage($type, $message) {
    return date("Ymd H:i:s") . " " . $type . $message;
  }
  
  protected function write($message) {
  }
}
