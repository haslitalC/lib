<?php

interface OsusLoggerInterface {
  public function info($message);
  public function warning($message);
  public function error($message);
}