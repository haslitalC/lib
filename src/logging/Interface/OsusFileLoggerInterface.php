<?php

interface OsusFileLoggerInterface extends OsusLoggerInterface {
  public function __construct(SplFileObject $file);
}