<?php

class OsusFileLogger implements OsusFileLoggerInterface {

  private $file = null;
  
  public function __construct(SplFileObject $file) {
    $this->file = $file;
  }
  
  public function info($message) {
    $message = $this->renderMessage("", $message);
    $this->write($message);
  }
  
  public function warning($message) {
    $message = $this->renderMessage("Warning: ", $message);
    $this->write($message);
  }
  
  public function error($message) {
    $message = $this->renderMessage("ERROR: ", $message);
    $this->write($message);
  }
  
  private function renderMessage($type, $message) {
    return date("Ymd H:i:s") . " " . $type . $message;
  }
  
  protected function write($message) {
    $fileobj = $this->file->openFile("a");
    $fileobj->fwrite($message . PHP_EOL);
  }
}
