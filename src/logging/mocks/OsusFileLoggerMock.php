<?php

class OsusFileLoggerMock extends OsusFileLogger {
  
  private $messages = array();
  private $lastMessage = "";
  
  protected function write($message) {
    $this->lastMessage = $message;
    $this->messages[] = $message;
  }
  
  public function getLastMessage() {
    return $this->lastMessage;
  }
  
  public function getMessages() {
    return $this->messages;
  }
}