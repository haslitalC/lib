<?php

class OsusLoggerMock extends OsusLogger {
  
  private $messages = array();
  private $lastMessage = "";
  
  protected function write($message) {
    $this->lastMessage = $message;
    $this->messages[] = $message;
    parent::write($message);
  }
  
  public function getLastMessage() {
    return $this->lastMessage;
  }
  
  public function getMessages() {
    return $this->messages;
  }
}
