<?php
/*******************************************************************************
*  COPYRIGHT 2014 by itsbusiness AG, Switzerland. All rights reserved.
* The Copyright to the computer program(s) herein is the property of
* itsbusiness AG, Switzerland. The program(s) may only be used and/or
* copied with the written permission from itsbusiness AG or in accordance
* with the terms and conditions stipulated in the agreement contract under
* which the program(s) have been supplied.
*
* $Id: livenet_sla_get_anschluss.php 6535 2014-03-26 09:55:21Z koto1 $
* $Revision: 6535 $
* $Date: 2014-03-26 10:55:21 +0100 (Wed, 26 Mar 2014) $
* $Author: koto1 $
*
*******************************************************************************/
/**
 * Database Helper, does some helpful things with databases
 *
 * @author Thorsten Koch
 * @since v1.0
 */
interface DatabaseHelperInterface
{
    
    const COMPRESSION_NONE = "none";
    const COMPRESSION_ZLIB = "zlib";
    const COMPRESSION_BZIP2 = "bzip2";
    
    /**
     * set the database connection
     *
     * @param DatabaseConnectionInterface $connection
     */
    public function __construct(DatabaseConnectionInterface $connection);
    
    
    /**
     * get the database connection
     *
     * @return DatabaseConnectionInterface
     */
    public function getConnection();
    
    /**
     * executes SQL from a file, compression is autodetected by the file extension:
     * <ul>
     * <li>gzip: .gz .gzip</li>
     * <li>bzip2: .bz2</li>
     * </ul>
     * For all other file extension we assume they are plain text. The compression set via setCompression does not
     * influence the behavior of this method.
     *
     * @param  string $absolute_filename
     * @param  string $schema The DB schema
     * @return bool success
     */
    public function runSQLFromFile($absolute_filename, $schema);
    
    /**
     * restores a dump from a file, compression is autodetected by the file extension:
     * <ul>
     * <li>gzip: .gz .gzip</li>
     * <li>bzip2: .bz2</li>
     * </ul>
     * For all other file extension we assume they are plain text. The compression set via setCompression does not
     * influence the behavior of this method.
     *
     * @param string $absolute_filename
     * @param string $database_name  name of the database to restore the dump in
     * @param bool success
     */
    public function restoreDumpFromFile($absolute_filename, $database_name = "");
    
    /**
     * dumps a single tables to a file
     *
     * @param  string $database_name  name of the database to dump
     * @param  array  $tables an array of all table names to be dumped
     * @param  string $absolute_filename
     * @return bool success
     */
    public function dumpTablesToFile($database_name, $tables, $absolute_filename);
    
    /**
     * dumps a database to a file
     *
     * @param  string $database_name  name of the database to dump
     * @param  string $absolute_filename
     * @return bool success
     */
    public function dumpDatabaseToFile($database_name, $absolute_filename);
    
    /**
     * Set the compression mode to be used for creating dumps. <code>COMPRESSION_NONE</code> has to be supported by
     * all implementations and must be the default. For all other modes the implementation must throw a
     * DatabaseException with code <code>COMPRESSION_NOT_SUPPORTED</code> if the mode is not supported.
     *
     * @param string $mode
     */
    public function setCompression($mode);
    
    /**
     * @return string The currently selected compression mode.
     */
    public function getCompression();
}
