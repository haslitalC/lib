<?php

/*******************************************************************************
*  COPYRIGHT 2014 by itsbusiness AG, Switzerland. All rights reserved.
* The Copyright to the computer program(s) herein is the property of
* itsbusiness AG, Switzerland. The program(s) may only be used and/or
* copied with the written permission from itsbusiness AG or in accordance
* with the terms and conditions stipulated in the agreement contract under
* which the program(s) have been supplied.
*
* $Id: DatabaseClientInterface.php 6535 2014-03-26 09:55:21Z koto1 $
* $Revision: 6535 $
* $Date: 2014-03-26 10:55:21 +0100 (Wed, 26 Mar 2014) $
* $Author: koto1 $
*
*******************************************************************************/
/**
 * interface, which describes the functionality of a database client
 *
 * @author Thorsten Koch
 * @since v1.0
 */
interface DatabaseClientInterface
{
    // the maximal query duration that does not generate a slow query log
    const SLOW_QUERY_LIMIT = 15; // ms
    const VERY_SLOW_QUERY_LIMIT = 1000; // ms
    const EXTREME_SLOW_QUERY_LIMIT = 2000; // ms

    /**
     * use a database in the database server
     *
     * @throws DatabaseConnectionException if selection of database failed, see USE_ERROR_* for reasons
     * @return bool
     */
    public function useDatabase($database);

    /**
     * returns the name of the database
     *
     * @return string
     */
    public function getDatabaseName();

    /**
     * sets the connection to the database server
     *
     * @param DatabaseConnectionInterface $connection
     */
    public function setConnection(DatabaseConnectionInterface $connection);

    /**
     * returns the used database connection object
     *
     * @return DatabaseConnectionInterface
     */
    public function getConnection();

    /**
     * returns the specific helper for this database server
     *
     * @return DatabaseHelperInterface
     */
    public function getHelper();

    public function query($sql, $check_performance = true);

    public function fetchRow();

    public function fetchArrayAssoc();

    public function fetchArray();

    public function fetchAll();

    public function numRows();

    public function affectedRows();

    public function freeLastResult();

    public function getLastErrorMessage();

    public function sanitize($value);
}
