<?php
/*******************************************************************************
*  COPYRIGHT 2014 by itsbusiness AG, Switzerland. All rights reserved.
* The Copyright to the computer program(s) herein is the property of
* itsbusiness AG, Switzerland. The program(s) may only be used and/or
* copied with the written permission from itsbusiness AG or in accordance
* with the terms and conditions stipulated in the agreement contract under
* which the program(s) have been supplied.
*
* $Id: livenet_sla_get_anschluss.php 6535 2014-03-26 09:55:21Z koto1 $
* $Revision: 6535 $
* $Date: 2014-03-26 10:55:21 +0100 (Wed, 26 Mar 2014) $
* $Author: koto1 $
*
*******************************************************************************/
/**
 * interface, which describes a connection to a database server
 *
 * @author Thorsten Koch
 * @since v1.0
 */
interface DatabaseConnectionInterface
{
    
    /**
     * constructor, does not initially connect
     *
     * @param string $server    the name of the database server
     * @param string $user      the name of the user, which is used to connect to the database server
     * @param string $password  the plain text password for the user
     */
    public function __construct($server, $user, $password);
    
    
    
    /**
     * connects to database
     *
     * @throws DatabaseConnectionException if connection could not be established, see CONNECT_ERROR_* for reasons
     * @return resource
     */
    public function connect();
    
    
    
    /**
     * disconnects from database
     *
     * @throws DatabaseConnectionException if connection could not be established, see DISCONNECT_ERROR_* for reasons
     * @return bool
     */
    public function close();
    
    
    
    /**
     * returns the specific helper for this database server
     *
     * @return DatabaseHelperInterface
     */
    public function getHelper();
    
    
    
    /**
     * returns the client to operate on the database
     *
     * @return DatabaseClientInterface
     */
    public function getClient();
    
    
    
    /**
     * Creates a second MysqlDb object that uses the same handle and returns it
     * This is required to be able to do nested SQL queries!
     *
     * @return DatabaseConnectionInterface
     */
//     public function duplicateConnection();



    /**
     * returns the name of the database server
     *
     * @return string
     */
    public function getServerName();
    
    
    
    /**
     * returns the name of the user, which is used to connect to the database server
     *
     * @return string
     */
    public function getUserName();
    
    
    
    /**
     * returns the plain text password of the user
     *
     * @return string
     */
    public function getUserPassword();
}
