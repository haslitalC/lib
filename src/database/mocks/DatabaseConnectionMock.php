<?php

class DatabaseConnectionMock implements DatabaseConnectionInterface {
  private $server;
    private $port = 3306;
  private $user;
  private $password;
  
    private $client = null;
    private $helper;
  
  
  public function __construct($server, $user, $password) {
        $this->server = $server;
        $this->user = $user;
        $this->password = $password;    
  }
  
    public function isConnected()
    {
        return true;
    }

  
  public function connect() {}
  public function close() {}
  public function getClient() {
    return new DatabaseClientMock($this);
  }
  public function getHelper() {
        if ($this->helper == null) {
            $this->helper = new MySQLHelper($this);
        }
        return $this->helper;
  }
  public function getServerName() {
    return $this->server;
  }
    public function getServerPort()
    {
        return $this->port;
    }

    public function getUserName() {
      return $this->user;
    }
    
    
    public function getUserPassword() {
      return $this->password;
    }
}
