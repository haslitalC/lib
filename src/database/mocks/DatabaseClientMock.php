<?php

class DatabaseClientMock extends MySQLClient
{
    // last query executed
    public $last_query;
    public $queries = array();
  
    private $rows = array();  
    private $lastInsertId = 1;
 
    /**
     * use a database in the database server
     *
     * @throws DatabaseConnectionException if selection of database failed, see USE_ERROR_* for reasons
     * @return bool
     */
    public function useDatabase($database) {}

    protected function connect() {
      return true;
    }
  
    public function query($sql, $check_performance = true) {
      $this->last_query = $sql;
      $this->queries[] = $sql;
      
      $this->lastInsertId++;
        
        $this->deleteEntry($sql);
    }
    
    private function deleteEntry($sql) {
        if (stripos($sql, "DELETE") === 0) {
            $id = substr(strstr($sql, "WHERE id ="), 10);
            foreach ($this->rows as $key=>$row) {
                if ($row["id"] == $id) {
                    unset($this->rows[$key]);
                }
            }
        }
    }
  
    public function lastInsertId() {
      return $this->lastInsertId;
    }
  
    /**
     * wird von fetchAll() zurückgegeben
     */
    public function setResult($rows) {
        $this->rows = $rows;
    }

    public function fetchRow() {}

    public function fetchArrayAssoc() {}

    public function fetchArray() {}

    public function fetchAll() {
        $result = array();
        
        foreach ($this->rows as $row) {
            $newRow = array();
            $columnCounter = 0;
            foreach($row as $value) {
                $newRow[$columnCounter] = $value;
            }
            $columnCounter++;
            $result[] = $newRow;
        }

        return $result;
    }

    public function fetchAllAssoc() {
        reset($this->rows);
        return $this->rows;
    }

    public function numRows() {
      return count($this->rows);
    }

    public function affectedRows() {}

    public function freeLastResult() {}

    /**
     * @TODO: we have to fake this
     */
    public function realEscapeString($string)
    {
        return $string;
    }
}
