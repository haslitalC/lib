<?php

/* * *****************************************************************************
 *  COPYRIGHT 2014 by itsbusiness AG, Switzerland. All rights reserved.
 * The Copyright to the computer program(s) herein is the property of
 * itsbusiness AG, Switzerland. The program(s) may only be used and/or
 * copied with the written permission from itsbusiness AG or in accordance
 * with the terms and conditions stipulated in the agreement contract under
 * which the program(s) have been supplied.
 *
 * $Id: MySQLConnection.php 678 2016-02-16 16:19:12Z koto1 $
 * $Revision: 678 $
 * $Date: 2016-02-16 17:19:12 +0100 (Tue, 16 Feb 2016) $
 * $Author: koto1 $
 *
 * ***************************************************************************** */

/**
 * Connection to a mySQL database
 *
 * @since  v1.0
 * @author Patrick Hämmerle
 *
 */
class MySQLConnection implements DatabaseConnectionInterface
{

    private $server;
    private $port = 3306;
    private $user;
    private $password;
    private $dbname;
    private $client = null;
    private $helper;

    /**
     * @param string $server   can be host name and port, like localhost:3306
     * @param string $user     name of the mysql user
     * @param string $password password for that mysql user
     */
    public function __construct($server, $user, $password)
    {
        $this->setServer($server);
        $this->user = $user;
        $this->password = $password;
    }

    public function __destruct()
    {
        if (!is_null($this->client)) {
            $this->close();
        }
    }

    public function isConnected()
    {
        return ($this->client != null ? true : false);
    }

    public function connect()
    {
        if ($this->client != null) {
            throw new DatabaseException(
                "Cannot connect when already connected",
                DatabaseException::CONNECT_ERROR_ALREADY_CONNECTED,
                null
            );
        }

        // we want to catch the error message of mysqli_connect internally
        try {
            $this->client = new MySQLClient($this);
        } catch (Exception $e) {
            throw new DatabaseException($e->getMessage(), DatabaseException::CONNECT_ERROR, null);
        }
    }

    public function close()
    {
        if ($this->client == null) {
            throw new DatabaseException(
                "Cannot close when not connected",
                DatabaseException::DISCONNECT_ERROR_NOT_CONNECTED,
                null
            );
        }
        $this->client = null;
    }

    /**
     * gets a client to operate on tables and stuff
     */
    public function getClient()
    {
        if ($this->client == null) {
            $this->connect();
        }
        return $this->client;
    }

    public function getHelper()
    {
        if ($this->helper == null) {
            $this->helper = new MySQLHelper($this);
        }
        return $this->helper;
    }

    /**
     * to prevent configuration changes from mysql to mysqli (serverstring must not include port)
     */
    private function setServer($server)
    {
        $arr = explode(":", $server);
        $this->server = $arr[0];
        if (isset($arr[1])) {
            $this->port = $arr[1];
        }
    }

    public function getServerName()
    {
        return $this->server;
    }

    public function getServerPort()
    {
        return $this->port;
    }

    public function getUserName()
    {
        return $this->user;
    }

    public function getUserPassword()
    {
        return $this->password;
    }
}
