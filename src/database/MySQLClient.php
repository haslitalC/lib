<?php

/* * *****************************************************************************
 *  COPYRIGHT 2014 by itsbusiness AG, Switzerland. All rights reserved.
 * The Copyright to the computer program(s) herein is the property of
 * itsbusiness AG, Switzerland. The program(s) may only be used and/or
 * copied with the written permission from itsbusiness AG or in accordance
 * with the terms and conditions stipulated in the agreement contract under
 * which the program(s) have been supplied.
 *
 * $Id: livenet_sla_get_anschluss.php 6535 2014-03-26 09:55:21Z koto1 $
 * $Revision: 6535 $
 * $Date: 2014-03-26 10:55:21 +0100 (Wed, 26 Mar 2014) $
 * $Author: koto1 $
 *
 * ***************************************************************************** */

/**
 * acts as decorator around MysqlDb
 *   must be implemented afterwards with full and new functionality
 */
class MySQLClient implements DatabaseClientInterface
{
    // last query executed
    private $last_query;

    private $connection = null;
    private $database = "";
    private $mysqli = null;
    private $result;
    private $connect_errno = 0;


    public function __construct(DatabaseConnectionInterface $connection = null)
    {
        if ($connection != null) {
            return $this->setConnection($connection);
        }
    }

    /**
     * sets the connection to the database server
     *
     * @param DatabaseConnectionInterface $connection
     */
    public function setConnection(DatabaseConnectionInterface $connection)
    {
        $this->connection = $connection;
        return $this->connect();
    }

    /**
     * returns the used database connection object
     *
     * @return DatabaseConnectionInterface
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * returns the specific helper for this database server
     *
     * @return DatabaseHelperInterface
     */
    public function getHelper()
    {
        return $this->connection->getHelper();
    }

    /**
     * 2015-12-07 gida1:
     * used in setConnection() and useDatabase() to ensure database is connected
     *
     * @return boolean if connect() is successful
     * @throws DatabaseException if connect() is not successful
     */
    protected function connect()
    {
        $this->mysqli = new mysqli(
            $this->connection->getServerName(),
            $this->connection->getUserName(),
            $this->connection->getUserPassword(),
            "",
            $this->connection->getServerPort()
        );

        if ($this->mysqli->connect_errno > 0) {
            throw new DatabaseException($this->errno, DatabaseException::CONNECT_ERROR, null);
        }
        return true;
    }

    public function useDatabase($database)
    {
        // init connection to the database, if not already done
        if ($this->mysqli == null) {
            $this->connect();
        }
        if ($this->mysqli->select_db($database)) {
            $this->database = $database;
            return true;
        } else {
            throw new DatabaseException($this->mysqli->errno, DatabaseException::USE_DATABASE_FAILED);
        }
    }

    public function getDatabaseName()
    {
        return $this->database;
    }

    /**
     * create database and optionally grant rights to an existing user
     *   this will only work on localhost servers
     *
     * @param  string  $database_name  name of the new database
     * @param  string  $dbo_username   name of the new database owner (existing user)
     */
    public function createDatabase($database_name, $username = "")
    {

        if ($this->mysqli->query("CREATE DATABASE `" . $database_name . "`;") == false) {
            $error_message = __METHOD__ . "(): " . $this->mysqli->error;
            throw new DatabaseException($error_message, $this->mysqli->errno, null);
        }
        if (!empty($username)) {
            $this->mysqli->query(
                "GRANT ALL ON " . $database_name . ".* TO '" . $username . "'@'localhost'"
            );
        }
        return true;
    }

    /**
     * drops database of given name
     *
     * @param string $database_name
     * @return bool
     * @throws DatabaseException
     */
    public function dropDatabase($database_name)
    {
        $success = $this->mysqli->query("DROP DATABASE `" . $database_name . "`");
        if ($success == false) {
            throw new DatabaseException($this->mysqli->error, $this->mysqli->errno);
        }
        return $success;
    }

    public function query($sql, $check_performance = true)
    {
        $time_start_run = microtime(true);

        $this->result = $this->mysqli->query($sql);
        if ($this->result == false) {
            throw new DatabaseException($this->mysqli->error, $this->mysqli->errno);
        }
        $this->last_query = $sql;

        // check for slow DB Queries!
        $this->checkPerformance($check_performance, (microtime(true) - $time_start_run) * 1000);

        return true;
    }

    public function fetchRow()
    {
        $records = $this->result->fetch_row();
        return $records;
    }

    public function fetchArrayAssoc()
    {
        $records = $this->result->fetch_array(MYSQLI_ASSOC);
        return $records;
    }

    public function fetchArray()
    {
        $records = $this->result->fetch_array();
        return $records;
    }

    public function fetchAll()
    {
        $all = array();

        while ($row = $this->fetchArray()) {
            array_push($all, $row);
        }

        return $all;
    }

    public function fetchAllAssoc()
    {
        $all = array();

        while ($row = $this->fetchArrayAssoc()) {
            array_push($all, $row);
        }

        return $all;
    }

    public function numRows()
    {
        return $this->result->num_rows;
    }

    public function affectedRows()
    {
        return $this->mysqli->affected_rows;
    }

    public function freeLastResult()
    {
        $this->result->free();
        return true;
    }

    public function getLastErrorMessage()
    {
        return $this->mysqli->error;
    }

    public function realEscapeString($string)
    {
        return $this->mysqli->real_escape_string($string);
    }

    /**
     * Sanitizes a value for insertion in the table
     *
     * SQL-escapes values and sets apostrophes on string values
     *
     * NOTE: The DAO concept here currently is not aware of column types,
     * therefore its up to the user to e.g. pass numbers as strings if
     * they need to be inserted into a CHAR/VARCHAR field (like DNs) and
     * vice-versa pass numbers as integers or floats if one wants to make
     * sure, that these values are inserted without single quotes (which
     * is perfectly save
     *
     * @param String $value
     *            Value to be sanitized
     * @return String Sanitized string
     *
     * 2015-12-03 gida1:
     *  copy-paste from MySqlDb.php
     */
    public function sanitize($value)
    {
        // convert null to SQL string representation
        if (is_null($value)) {
            $value = 'null';
        } elseif (is_bool($value)) {
            $value = $value ? '1' : '0';
        } elseif (is_int($value) || is_float($value)) {
            $value = strval($value);
        } else {
            // we consider everything else needed to be in single quotes for
            // insertion into the database. also all these values are
            // potentially harmful and therefore are escaped properly.
            //
            // consider the following scenario: numbers (integers or floats)
            // are passed as strings to the save method - this happens when a
            // record is read by find_by... and such attributes are written
            // back to the database (save) *untouched*. in this case, it is
            // considered acceptable that these values are inserted *with*
            // single quotes. in such cases MySQL needs to typecast the
            // string back to the respective number type and has a little
            // preformance impact. apart from this there should be no cases
            // where this has any negative effect and it fixes the problem
            // described in
            // @see http://bugzilla.itp.intranet/show_bug.cgi?id=1025
            // where we encountered problems with enum fields that defined
            // numbers as strings as ennumeration values.

            $value = $this->realEscapeString($value);

            $value = "'" . $value . "'";
        }
        return $value;
    }

    /**
     * Checks performance of latest executed query
     *
     * @TODO: We have no logger, how should we report slow queries?
     *
     * @param bool $check_performance
     *            check performance
     * @param int $timeMs
     *            runtime of query in microseconds
     */
    protected function checkPerformance($check_performance, $timeMs)
    {
        if ($check_performance) {
            /*
            if ($timeMs > self::EXTREME_SLOW_QUERY_LIMIT) {
                $message = "SLOW-SQL: Extreme slow SQL query detected: " . $timeMs . " ms for " . $this->last_query .
                ";";

                $this->log->write(Logger::ERROR, $message);
            } elseif ($timeMs > self::VERY_SLOW_QUERY_LIMIT) {
                $message = "SLOW-SQL: Very slow SQL query detected: " . $timeMs . " ms for " . $this->last_query . ";";

                $this->log->write(Logger::WARN, $message);
            } elseif ($timeMs > self::SLOW_QUERY_LIMIT) {
                $message = "SLOW-SQL: Slow SQL query detected: " . $timeMs . " ms " . " for " . $this->last_query . ";";

                $this->log->write(Logger::PERF, $message);
            }
            */
        }
    }
  
  
  
		public function insertQuery($table, $fields) {
			$values=$keys=array();
			foreach($fields as $key=>$val) {
				$values[]=$val;
				$keys[]=$key;
			}

			return 'INSERT INTO ' . $table . ' ('. implode(', ', $keys). ') VALUES (' . implode(', ', $values) . ')';
		}
	
	
	
		/**
		 * Liefert die zuletzt ge�nderte ID eines UPDATEs zur�ck
		 *   Falls nicht m�glich, wird null zur�ckgegeben
		 */
		public function lastInsertId() {
			if(!is_null($this->mysqli)) {
				return $this->mysqli->insert_id;
			}
			return null;		
		}	
}
