<?php

/*******************************************************************************
*  COPYRIGHT 2014 by itsbusiness AG, Switzerland. All rights reserved.
* The Copyright to the computer program(s) herein is the property of
* itsbusiness AG, Switzerland. The program(s) may only be used and/or
* copied with the written permission from itsbusiness AG or in accordance
* with the terms and conditions stipulated in the agreement contract under
* which the program(s) have been supplied.
*
* $Id: livenet_sla_get_anschluss.php 6535 2014-03-26 09:55:21Z koto1 $
* $Revision: 6535 $
* $Date: 2014-03-26 10:55:21 +0100 (Wed, 26 Mar 2014) $
* $Author: koto1 $
*
*******************************************************************************/
/**
 * Exception thrown by DatabaseConnection classes
 * - when connection could not be established
 * - when connection could not be closed
 * - when database could not be selected
 *
 * @since v1.0
 * @author Thorsten Koch
 */
class DatabaseException extends RuntimeException
{

    const DATABASE_ERROR = 0;
    
    const CONNECT_ERROR = 1;

    const CONNECT_ERROR_ALREADY_CONNECTED = 2;

    const DISCONNECT_ERROR_NOT_CONNECTED = 3;

    /**
     * Dumps and restore without password are not allowed.
     */
    const DUMP_WITH_EMPTY_PASSWORD = 4;
    const RESTORE_WITH_EMPTY_PASSWORD = 5;
    
    const USE_DATABASE_FAILED = 6;
    
    const DUMP_WITH_INVALID_FILE = 7;
    const RESTORE_WITH_INVALID_FILE = 8;
    
    const COMPRESSION_NOT_SUPPORTED = 9;
    
    const DUMP_TABLES_MISSING_TABLE_NAMES = 10;
}
