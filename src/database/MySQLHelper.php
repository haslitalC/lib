<?php

/*******************************************************************************
*  COPYRIGHT 2014 by itsbusiness AG, Switzerland. All rights reserved.
* The Copyright to the computer program(s) herein is the property of
* itsbusiness AG, Switzerland. The program(s) may only be used and/or
* copied with the written permission from itsbusiness AG or in accordance
* with the terms and conditions stipulated in the agreement contract under
* which the program(s) have been supplied.
*
* $Id: MySQLHelper.php 470 2015-07-15 07:37:48Z koto1 $
* $Revision: 470 $
* $Date: 2015-07-15 09:37:48 +0200 (Wed, 15 Jul 2015) $
* $Author: koto1 $
*
*******************************************************************************/

/**
 * Helper Class for mySQL Database
 * - Dump and restore of a database
 *
 * @since v1.0
 * @author Patrick Hämmerle
 *
 */
class MySQLHelper implements DatabaseHelperInterface
{

    private $connection;
    private $compression = DatabaseHelperInterface::COMPRESSION_NONE;
    
    private $compressionModes = array(
        DatabaseHelperInterface::COMPRESSION_NONE => "",
        DatabaseHelperInterface::COMPRESSION_ZLIB => "compress.zlib://",
        DatabaseHelperInterface::COMPRESSION_BZIP2 => "compress.bzip2://"
    );

    public function __construct(DatabaseConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function getConnection()
    {
        return $this->connection;
    }
    
    public function runSQLFromFile($absolute_filename, $schema)
    {
        if ($schema == null || trim($schema) == "") {
            throw new DatabaseException("Schema may not be null or empty", DatabaseException::USE_DATABASE_FAILED);
        }
        /* Check if the schema is valid */
        $this->getConnection()->getClient()->useDatabase($schema);
        
        return $this->runSQL($absolute_filename, $schema);
    }

    public function restoreDumpFromFile($absolute_filename, $database_name = "")
    {
        return $this->runSQL($absolute_filename, $database_name);
    }

    public function dumpTablesToFile($database_name, $tables, $absolute_filename)
    {
        if ($tables == null || count($tables) < 1) {
            throw new DatabaseException("Tables must be specified", DatabaseException::DUMP_TABLES_MISSING_TABLE_NAMES);
        }
        return $this->dump($database_name, $tables, $absolute_filename);
    }
    
    public function dumpDatabaseToFile($database_name, $absolute_filename)
    {
        return $this->dump($database_name, null, $absolute_filename);
    }
    
    public function setCompression($mode)
    {
        if (array_key_exists($mode, $this->compressionModes)) {
            $this->compression = $mode;
        } else {
            throw new DatabaseException(
                "Compression mode <" . $mode . "> not supported",
                DatabaseException::COMPRESSION_NOT_SUPPORTED
            );
        }
    }
    
    public function getCompression()
    {
        return $this->compression;
    }
    
    private function dump($database_name, $tables, $absolute_filename)
    {
        if ($this->connection->getUserPassword() != "") {
            $marker = ((PHP_OS == "Linux") ? "'" : "");
            $pw = " -p" . $marker . $this->connection->getUserPassword() . $marker;
        } else {
            throw new DatabaseException(
                "Dump without password not allowed",
                DatabaseException::DUMP_WITH_EMPTY_PASSWORD
            );
        }
        $stderr = tempnam(sys_get_temp_dir(), "dumpDatabase_stderr_");
    
        @touch($absolute_filename);
        if (!is_writable($absolute_filename)) {
            throw new DatabaseException(
                "Unable to create file: '" . $absolute_filename . "'",
                DatabaseException::DUMP_WITH_INVALID_FILE
            );
        }
    
        $command = "";
        if ($tables == null) {
            $command = "mysqldump -u " .
                $this->connection->getUserName() .
                $pw .
                " " .
                "--skip-lock-tables --quick " .
                "--add-drop-database --routines --databases --default-character-set=utf8 " .
                $database_name .
                " 2> \"" .
                realpath($stderr) . "\"";
        } else {
            $tableString = "";
            foreach ($tables as $table) {
                $tableString .= " " . $table;
            }
            $command = "mysqldump -u " .
                $this->connection->getUserName() .
                $pw .
                " " .
                "--lock-all-tables --quick " .
                "--add-drop-table --routines --default-character-set=utf8 " .
                $database_name . $tableString . " " .
                " 2> \"" .
                realpath($stderr) . "\"";
        }
    
        $commandOutput = popen($command, 'r');
    
        $fileOutput = null;
        $fileOutput = fopen($this->compressionModes[$this->compression] . realpath($absolute_filename), "w");
        while (!feof($commandOutput)) {
            $buffer = fread($commandOutput, 1024);
            fwrite($fileOutput, $buffer);
        }
        fclose($fileOutput);
        fclose($commandOutput);
    
        $stderr_str = "Unknown Database Error";
        $filesize = filesize($stderr);
        $exception = null;
        if ($filesize > 0) {
            $handle = fopen($stderr, "r");
            $stderr_str = fread($handle, $filesize);
            fclose($handle);
            $exception = new DatabaseException($stderr_str, DatabaseException::DATABASE_ERROR);
        }
    
        if (file_exists($stderr)) {
            unlink($stderr);
        }
        if ($exception != null) {
            throw $exception;
        }
        return (true);
    }
    
    private function runSQL($absolute_filename, $schema = "")
    {
        /*
         * Check Input parameters
         */
        if (!is_readable($absolute_filename)) {
            throw new DatabaseException("Cannot open the file.", DatabaseException::RESTORE_WITH_INVALID_FILE);
        }
        
        /*
         * Create Command
         */
        if ($this->connection->getUserPassword() != "") {
            $marker = ((PHP_OS == "Linux") ? "'" : "");
            $pw = " -p" . $marker . $this->connection->getUserPassword() . $marker;
        } else {
            throw new DatabaseException(
                "Restore without password not allowed",
                DatabaseException::RESTORE_WITH_EMPTY_PASSWORD
            );
        }
        
        $stderr = tempnam(sys_get_temp_dir(), "restoreDump_stderr_");
        $stdout = tempnam(sys_get_temp_dir(), "restoreDump_stdout_");
        $command = "mysql -u " . $this->connection->getUserName() . " " .
            $pw . " " . $schema . " 1> \"" . realpath($stdout) . "\" 2> \"" . realpath($stderr) . "\"";
        
        /*
         * Check file extension/filetype
         */
        if (substr($absolute_filename, strlen($absolute_filename) - 3) == ".gz" ||
           substr($absolute_filename, strlen($absolute_filename) - 5) == ".gzip") {
            $absolute_filename =
                $this->compressionModes[DatabaseHelperInterface::COMPRESSION_ZLIB] . $absolute_filename;
        } elseif (substr($absolute_filename, strlen($absolute_filename) - 4) == ".bz2" ||
            substr($absolute_filename, strlen($absolute_filename) - 5) == ".gzip") {
            $absolute_filename =
                $this->compressionModes[DatabaseHelperInterface::COMPRESSION_BZIP2] . $absolute_filename;
        }
        
        /*
         * Run the command
         */
        $fp = fopen($absolute_filename, "r");
        $commandInput = popen($command, "w");
        while (!feof($fp)) {
            $buffer = fread($fp, 1024);
            fwrite($commandInput, $buffer);
        }
        fclose($fp);
        fclose($commandInput);
        
        /*
         * Check for errors
         */
        $stderr_str = "Unknown Database Error";
        $filesize = filesize($stderr);
        if ($filesize > 0) {
            $handle = fopen($stderr, "r");
            $stderr_str = fread($handle, $filesize);
            fclose($handle);
            if (file_exists($stderr)) {
                unlink($stderr);
            }
            if (file_exists($stdout)) {
                unlink($stdout);
            }
            throw new DatabaseException($stderr_str, DatabaseException::DATABASE_ERROR);
        }
        
        /*
         * Cleanup and return
         */
        if (file_exists($stderr)) {
            unlink($stderr);
        }
        if (file_exists($stdout)) {
            unlink($stdout);
        }
        
        return true;
    }
}
