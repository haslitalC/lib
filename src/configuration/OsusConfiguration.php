<?php

class OsusConfiguration {

  static private $instance = null;
  
  public $readfiles = array();
  public $values = array();
  
  static public function getInstance($pathToConfigFiles = null) {
    if (is_null(self::$instance)) {
      if (is_null($pathToConfigFiles)) {
        throw new Exception('Singleton not created yet, $pathToConfigFiles parameter is mandatory');
      }
      
      $config = new self();
      $config->loadConfigs($pathToConfigFiles);
      self::$instance = $config;
    } else {
      if (!is_null($pathToConfigFiles)) {
        self::$instance->loadConfigs($pathToConfigFiles);
      }
    }
    
    return self::$instance;
  }
  
  
  
  private function __construct() {    
  }

  
  
  public function get($key) {
    return $this->values[$key];
  }
  
  
  
  public function loadConfigs($pathToConfigFiles) {
    $dir = new FileSystemIterator($pathToConfigFiles, FilesystemIterator::CURRENT_AS_FILEINFO && FilesystemIterator::SKIP_DOTS);
    
    foreach($dir as $fileInfo) {
      if ($fileInfo->isDir()) {
        continue;
      }
      
      if (substr($fileInfo->getBasename(), 0, 1) == ".") {
        continue;
      }
      
      $ext = $fileInfo->getExtension();
      switch($ext) {
        case "json":
          $this->readFromJsonFile($fileInfo);
          break;
        case "properties":
          $this->readFromPropertiesFile($fileInfo);
          break;
        default:
          throw new Exception("invalid file, cannot read file with extension \"" . $ext . "\"");
      }
    }
  }

  
  
  private function readFromJsonFile(SplFileInfo $file) {
    $content = file_get_contents($file->getPathname());
    $parameters = json_decode($content, true);
    
    foreach ($parameters as $key => $value) {
      $this->setConfigValue($key, $value);
    }
    
    $this->readFiles[] = $file->getPathname();
    
    return true;
  }
  

  
  private function readFromPropertiesFile(SplFileInfo $file) {
    $parameters = parse_ini_file($file->getPathname());
    
    foreach ($parameters as $key => $value) {
      $this->setConfigValue($key, $value);
    }
    
    $this->readFiles[] = $file->getPathname();
    
    return true;
  }

  
  
  private function setConfigValue($key, $value) {

    if (is_array($value)) {
      foreach($value as $subKey => $subValue)  {
        $this->setConfigValue($key . "." . $subKey, $subValue);
      }
    } else {
      $this->values[$key] = $value;
    }
  }
}