<?php

interface OSUS_DBInterface {


	/**
	 * Charset der Verbindung setzen
	 */
  public function initCharset();
  
  
  
	/**
		* Liest Daten aus einer Datenbanktabelle
		* ESCAPED NICHT!
		* 
		* @param  string  table   Name der zu lesenden Tabelle
		* @param  string  colums  Felder, die aus der Tabelle gelesen werden sollen
		* @param  mixed   where   WHERE Clause des Selects, string oder array
		* @param  string  ext     restliche SQL-Anweisungen nach WHERE
		*
		* @return array   ausgelesene Datensätze
		*/
	public function select($table, $columns='*', $where='', $ext='');
  
  
  
	/**
	 * Schreibt Daten in eine Tabelle
	 * ESCAPED NICHT!
	 * 
	 * @param  string   table   Name der zu schreibenden Tabelle
	 * @param  array    set     Einzufügende Daten (Feldname und Wert)
	 * @param  string   ext     restliche SQL-Anweisungen nach WHERE   
	 */
	public function insert($table, $set, $ext='');
  
  
	/**
	 * Ersetzt Daten in einer Tabelle
	 * ESCAPED NICHT!
	 * 
	 * @param  string   table   Name der zu schreibenden Tabelle
	 * @param  array    set     Einzufügende Daten (Feldname und Wert)
	 * @param  string   ext     restliche SQL-Anweisungen nach WHERE   
	 */
	public function replace($table, $set, $ext='');
  

  
	/**
	 * Überschreibt Datensätze und nimmt an, dass sie meistens schon vorhanden 
	 *   sind
	 * 
	 * Rückgabewert: 
	 *   War Datensatz schon vorhanden und wurde überschrieben?
	 * 
	 * @param   string   table
	 * @param   mixed    set               Schon escapted Spalten und Werte
	 * @param   string   primaryKeyColumn  Name der Spalte des PrimaryKeys   
	 * 
	 * @throws Exception
	 * @return bool
	 */
	public function overwrite($table, $set, $primaryKeyColumn);
  
    
	/**
	* Liest Daten aus einer Datenbanktabelle
	*
	* @param  string  table   Name der zu lesenden Tabelle
	* @param  string  select  Felder, die aus der Tabelle gelesen werden sollen
	* @param  mixed   where   WHERE Clause des Selects, string oder array
	* @param  string  ext     restliche SQL-Anweisungen nach WHERE
	*
	* @return array   ausgelesene Datens�tze
	*/
	public function dbGet($table, $select='*', $where='', $ext='');    
  
  public function dbDelete($table, $where='');

	/**
	* Sende ein Query an die Datenbank
	* 
	* R�ckgabewert: 
	* Ein Array mit den Zeilen, bzw. ein leeres Array bei Misserfolg
	*
	* @param  string query     database query
	* 
	* @return mixed
	*/
	public function dbQuery($query);
  
  
	/**
	 * Schreibt Daten in eine Tabelle
	 *   Werte in set und where werden escaped
	 * 
	 * @param  string   table   Name der zu schreibenden Tabelle
	 * @param  array    set     Einzuf�gende Daten (Feldname und Wert)
	 * @param  mixed    where   WHERE Clause des Selects, string oder array
	 * @param  string   ext     restliche SQL-Anweisungen nach WHERE   
	 */
	public function dbSet($table, $set, $where='', $ext='');
  
  
	/**
	 * Ändert Daten in einer Tabelle
	 * ESCAPED NICHT!
	 *
	 * @param  string   table   Name der zu schreibenden Tabelle
	 * @param  array    set     Einzufügende Daten (Feldname und Wert)
	 * @param  mixed    where   WHERE Clause des Selects, string oder array
	 * @param  string   ext     restliche SQL-Anweisungen nach WHERE
	 */
	public function update($table, $set, $where='', $ext='');  

  
  
  
	/**
	 * Löscht einen Datensatz in einer Tabelle
	 * ESCAPED NICHT!
	 *
	 * @param  string   table   Name der zu schreibenden Tabelle
	 * @param  array    set     Einzufügende Daten (Feldname und Wert)
	 * @param  mixed    where   WHERE Clause des Selects, string oder array
	 * @param  string   ext     restliche SQL-Anweisungen nach WHERE
	 */
	public function delete($table, $where='', $ext='');
  
  
	/**
	 * Liefert die zuletzt ge�nderte ID eines UPDATEs zur�ck
	 *   Falls nicht m�glich, wird null zur�ckgegeben
	 */
	public function lastInsertId();

  
  
	/**
	 * Liefert die Anzahl der zuletzt ge�nderten Datens�tze
	 */
	public function affectedRows();

  
	/**
	* SQL: Bastelt einen SQL-Insert Statement Teil aus einem 1D-Assoz-Array
	*      Die ' bei den Values m�ssen selbst gesetzt werden!
	*
	* @param    array       Der Key gibt den Feldnamen an
	*                       Der Value gibt den Feldinhalt an
	*
	* @return          Liefert einen SQL String wie
	*                      "(feld1, feld5, feld3) VALUES('value1', 'value5'. 'value3')"
	*
	*/
	static public function sqlInsert($array);
  
  
  public function escapeBool($bool);

  
  
}