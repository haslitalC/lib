<?php
/**
 * MYSQLi Datenbankverbindung
 */
class OSUS_DBmysqli extends OSUS_DBmysqlAbstract {
	/**
	 * Liefert die Connection zur Datenbank zur�ck
	 */
	protected function getDbConnection() {
		if($this->dbConnection!=null) return $this->dbConnection;

	  $this->dbConnection = new mysqli($this->host['host'], $this->host['user'], 
   		$this->host['pw'], $this->host['db'], $this->host['port']);		
		if (mysqli_connect_errno()) {
		    echo 'Connect failed: ', mysqli_connect_error();
		    exit();
		}		
		
		$this->initCharset();
		return $this->dbConnection;
	}


	
	/**
	* Sende ein Query an die Datenbank
	* 
	* R�ckgabewert: 
	* Ein Array mit den Zeilen, bzw. ein leeres Array bei Misserfolg
	*
	* @param  string query     database query
	* 
	* @return array
	*/
	public function dbQuery($query) {

	  if($connection = $this->getDbConnection()) {

if(ECHO_ON) echo nl2br(microdate('H:i:s').' '.__CLASS__.'->dbQuery(): '.$query.PHP_EOL);

		  $result = $connection->query($query, MYSQLI_USE_RESULT);
      $this->lastQuery=$query;
		  if($result!==false) {
				if(strpos($query, 'INSERT')===0) {
					return $connection->insert_id;
				}		  
		    $rows=array();
		    while ($row = $result->fetch_assoc()) {
		        $rows[]=$row;
		    }
			  return $rows;
		  }
		  else {
		  	trigger_error('SQL_Statement: '.$query, E_USER_WARNING);
		  	return array();
		  }	  	
	  }
	  
	  return array();
	}



	/**
	 * Liefert die zuletzt ge�nderte ID eines UPDATEs zur�ck
	 *   Falls nicht m�glich, wird null zur�ckgegeben
	 */
	public function lastInsertId() {
	  if($connection = $this->getDbConnection()) {
	  	return $connection->insert_id;
	  }
	  return null;		
	}	

	/**
	 * Liefert die Anzahl der zuletzt ge�nderten Datens�tze
	 */
	public function affectedRows() {
	  if($connection = $this->getDbConnection()) {
	  	return $connection->affected_rows;
	  }
	  return null;		
	}	
}
?>