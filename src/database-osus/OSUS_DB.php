<?php
/**
 * Abstrakte Klasse f�r alle Datenbankobjekte
 */
abstract class OSUS_DB  {

	/** Datenbankverbindung */
	protected $dbConnection=null;

	
	/** Konfiguration der Datenbankverbindung */
	protected $host=array();


	/** Singleton Datenbankconnections Objekte je nach Typ */
	static private $connections = array();

	public $lastQuery='';

	/**
	 * Factory: Liefert eine DB-Objekt je nach Datenbanktyp zur�ck
	 * 
	 * @param    array   dbOverride  Alternative DB Konfiguration 
	 */
	final static function getInstance($dbOverride='') {
		$db = OSUS_Registry::get('databaseConnection');
		if(!empty($dbOverride)) $db = $dbOverride;
	
		$type = $db['type'].$db['driver']; 
		if(isset(self::$connections[$type])) return self::$connections[$type];

		switch(strtolower($db['type'])) {
			case 'mysql':
				switch(strtolower($db['driver'])) {
					case 'mysqli':
						$className = 'OSUS_DBmysqli';
					break;
					case 'pdo':
						$className = 'OSUS_DBmysqlPDO';
					break;
					default:
            throw new Exception("unknown driver " . $db['driver']);
//						$className = 'OSUS_DBmysql';
//					break;
				}
        $obj = new $className($db);
				break;
			default:
		}
		
		self::$connections[$type] = $obj;
		return $obj;
	}


	/**
	 * Liefert die Datenbankverbindung zur�ck, wenn vorhanden, oder etabliert sie.
	 */
	abstract protected function getDbConnection();


	/**
	 * Liefert die zuletzt ge�nderte ID eines UPDATEs zur�ck
	 *   Falls nicht m�glich, wird null zur�ckgegeben
	 */
	abstract public function lastInsertId();

	/**
	 * Liefert die Anzahl der zuletzt ge�nderten Datens�tze
	 */
	abstract public function affectedRows();




	/**
	 * Überschreibt alte Zeilen mit neuen Zeilen und löscht evtl. überschüssige
	 *   weg bzw. legt neue an
	 * 
	 * Tabelle braucht als Primary Key die Spalte ID
	 * 
	 * ACHTUNG! Alle Datenfelder müssen in $new vorhanden sein, damit sie bei 
	 *   einem Update überschrieben werden. Sonst werden falsche Daten generiert!
	 * 
	 * @param  string   table   Tabellennamen 
	 * @param  array    old     Alte zu ersetzende Datensätze
	 * @param  array    new     Neue Daten, die geschrieben werden sollen
	 */
	public function replaceRows($table, $old, $new) {
		$rowcount=0;
		while(isset($old[$rowcount]) or isset($new[$rowcount])) {
			// Keine neue mehr, also alte Zeile löschen
			if(!isset($new[$rowcount])) {
				$this->dbDelete($table, $old[$rowcount]['ID']);				
			}
			// Alter DS nicht mehr vorhanden, also neuen Datensatz erstellen
			else if(!isset($old[$rowcount])) {
				if(isset($new[$rowcount]['ID'])) unset($new[$rowcount]['ID']);
				$this->dbSet($table, 
					$this->sqlInsertUnescaped($this->escapeString($new[$rowcount])));	
			}
			else { // Beide noch vorhanden, ersetzen!
				$this->dbSet($table, 
					$this->sqlUpdateUnescaped($this->escapeString($new[$rowcount])), 
					array('ID'=>$old[$rowcount]['ID']));		
			}

			$rowcount++;
		}
	}
	
	public function escapeBool($bool)
	{
		return ($bool === true) ? 1 : 0;
	}
}
?>