<?php
/**
 * Abstrakte Klasse f�r den Zugriff auf eine mySQL Datenbank
 * Bietet f�r alle mySQL Klassen die mySQL Syntax an
 */
abstract class OSUS_DBmySQLAbstract extends OSUS_DB implements OSUS_DBInterface {


	/** Anzahl, die per bulk insert in eine Tabelle eingef�gt werden k�nnen */
	protected $DB_BULKINSERT=5000;


	public function __construct($dbOverride='') {
		
		$db = OSUS_Registry::get('databaseConnection');
		if(!empty($dbOverride)) $db = $dbOverride;
		$this->host   = $db;
	}



	/**
	 * Charset der Verbindung setzen
	 */
	public function initCharset() {
		$this->dbQuery('SET NAMES \'utf8\'');
		$this->dbQuery('SET CHARACTER SET utf8');
//		$this->dbQuery('SET NAMES \'latin2\'');
//		$this->dbQuery('SET CHARACTER SET latin2');
//		debug($this->dbQuery('SHOW VARIABLES LIKE \'character_set_server%\''));		
//		debug($this->dbQuery('SHOW VARIABLES LIKE \'collation_server%\''));
//		exit;		
	}

	

	/**
		* Liest Daten aus einer Datenbanktabelle
		* ESCAPED NICHT!
		* 
		* @param  string  table   Name der zu lesenden Tabelle
		* @param  string  colums  Felder, die aus der Tabelle gelesen werden sollen
		* @param  mixed   where   WHERE Clause des Selects, string oder array
		* @param  string  ext     restliche SQL-Anweisungen nach WHERE
		*
		* @return array   ausgelesene Datensätze
		*/
	final public function select($table, $columns='*', $where='', $ext='') {
	  $command='SELECT '.$columns.' FROM '.$table;

	  if(is_array($where))
	  	$where=' WHERE '.self::sqlWhereUnescaped($where);
	  else $where=' WHERE '.$where;

	  return $this->dbQuery($command.' '.$where.' '.$ext);
	}
	
	
	
	/**
	 * Schreibt Daten in eine Tabelle
	 * ESCAPED NICHT!
	 * 
	 * @param  string   table   Name der zu schreibenden Tabelle
	 * @param  array    set     Einzufügende Daten (Feldname und Wert)
	 * @param  string   ext     restliche SQL-Anweisungen nach WHERE   
	 */
	final public function insert($table, $set, $ext='') {
		$command = 'INSERT INTO '.$table.' '.self::sqlInsertUnescaped($set);
	  return $this->dbQuery($command.' '.$ext);
	}


	
	/**
	 * Ersetzt Daten in einer Tabelle
	 * ESCAPED NICHT!
	 * 
	 * @param  string   table   Name der zu schreibenden Tabelle
	 * @param  array    set     Einzufügende Daten (Feldname und Wert)
	 * @param  string   ext     restliche SQL-Anweisungen nach WHERE   
	 */
	final public function replace($table, $set, $ext='') {
		$command = 'REPLACE '.$table.' '.self::sqlInsertUnescaped($set);
	  return $this->dbQuery($command.' '.$ext);
	}


	
	/**
	 * Ändert Daten in einer Tabelle
	 * ESCAPED NICHT!
	 *
	 * @param  string   table   Name der zu schreibenden Tabelle
	 * @param  array    set     Einzufügende Daten (Feldname und Wert)
	 * @param  mixed    where   WHERE Clause des Selects, string oder array
	 * @param  string   ext     restliche SQL-Anweisungen nach WHERE
	 */
	final public function update($table, $set, $where='', $ext='') {
		$command = 'UPDATE '.$table.' SET '.self::sqlUpdateUnescaped($set);
		if(is_array($where))
			$where=' WHERE '.self::sqlWhereUnescaped($where);
		else $where=' WHERE '.$where;
	
		return $this->dbQuery($command.' '.$where.' '.$ext);
	}
	
	
	
	/**
	 * Löscht einen Datensatz in einer Tabelle
	 * ESCAPED NICHT!
	 *
	 * @param  string   table   Name der zu schreibenden Tabelle
	 * @param  array    set     Einzufügende Daten (Feldname und Wert)
	 * @param  mixed    where   WHERE Clause des Selects, string oder array
	 * @param  string   ext     restliche SQL-Anweisungen nach WHERE
	 */
	final public function delete($table, $where='', $ext='') {
		$command = 'DELETE FROM '.$table;
		if(is_array($where))
			$where=' WHERE '.self::sqlWhereUnescaped($where);
		else $where=' WHERE '.$where;
	
		return $this->dbQuery($command.' '.$where.' '.$ext);
	}
	
	

	/**
	 * Überschreibt Datensätze und nimmt an, dass sie meistens schon vorhanden 
	 *   sind
	 * 
	 * Rückgabewert: 
	 *   War Datensatz schon vorhanden und wurde überschrieben?
	 * 
	 * @param   string   table
	 * @param   mixed    set               Schon escapted Spalten und Werte
	 * @param   string   primaryKeyColumn  Name der Spalte des PrimaryKeys   
	 * 
	 * @throws Exception
	 * @return bool
	 */
	public function overwrite($table, $set, $primaryKeyColumn) {
		$withoutPrimary = $set;
		unset($withoutPrimary[$primaryKeyColumn]);
		$where = array($primaryKeyColumn=>$set[$primaryKeyColumn]);
		$this->set($table, $withoutPrimary, $where);
		
		$affected = $this->affectedRows();
		if(is_numeric($affected) and $affected!=0) return true;

		$this->set($table, $set);
		return false;
	}









	/**
	* Liest Daten aus einer Datenbanktabelle
	*
	* @param  string  table   Name der zu lesenden Tabelle
	* @param  string  select  Felder, die aus der Tabelle gelesen werden sollen
	* @param  mixed   where   WHERE Clause des Selects, string oder array
	* @param  string  ext     restliche SQL-Anweisungen nach WHERE
	*
	* @return array   ausgelesene Datens�tze
	*/
	final public function dbGet($table, $select='*', $where='', $ext='') {
	  $query='SELECT '.$select.' FROM '.$table;
	  if(is_array($where)) {
		  if(!empty($where)) 
		  	$query.=' WHERE '.self::sqlWhere($where);
	  } 
	  else if($where!='')  $query.=' WHERE '.$where;
	  return $this->dbQuery($query.' '.$ext);
	}


	/**
	 * Schreibt Daten in eine Tabelle
	 *   Werte in set und where werden escaped
	 * 
	 * @param  string   table   Name der zu schreibenden Tabelle
	 * @param  array    set     Einzuf�gende Daten (Feldname und Wert)
	 * @param  mixed    where   WHERE Clause des Selects, string oder array
	 * @param  string   ext     restliche SQL-Anweisungen nach WHERE   
	 */
	final public function dbSet($table, $set, $where='', $ext='') {
		if(empty($where)) {
			$command = 'INSERT INTO '.$table.' '.self::sqlInsert($set);
			$where='';
		}
		else {
			$command = 'UPDATE '.$table.' SET '.self::sqlUpdate($set);
			if(is_array($where)) 
				$where=' WHERE '.self::sqlWhere($where);
	  	else $where=' WHERE '.$where;
		}		

	  return $this->dbQuery($command.' '.$where.' '.$ext);
	}
	
	/**
	 * Massen "insert" vieler Zeilen
	 */
	function dbBulkSet($table, $rows) {
		$count=1;
		$sql='';
		$keyString='('.implode(', ', array_keys($rows[0])).') ';
		foreach($rows as $row) {
			$sql.='("'.implode('", "', $row).'"), ';
			if(!($count%$this->DB_BULKINSERT)) {
				$sql='INSERT INTO '.$table.' '.$keyString.' VALUES '.$sql;			
				$sql=substr($sql, 0, strlen($sql)-2);
				$x = $this->dbQuery($sql);
				$sql='';
			}
			$count++;
		}
	
		// Den Rest auch noch einfügen
		if(strlen($sql)) {
			$sql='INSERT INTO '.$table.' '.$keyString.' VALUES '.$sql;			
			$sql=substr($sql, 0, strlen($sql)-2);
			$x = $this->dbQuery($sql);
		}
	}
	
	final public function dbDelete($table, $where='') {
		$command = 'DELETE FROM '.$table;
		if(!empty($where)) {
			if(is_array($where)) 
				$where=' WHERE '.self::sqlWhere($where);
	  	else $where=' WHERE '.$where;
			$command.=$where;
		}
	  return $this->dbQuery($command);
	}
	
	/**
	* Sende ein Query an die Datenbank
	* 
	* R�ckgabewert: 
	* Ein Array mit den Zeilen, bzw. ein leeres Array bei Misserfolg
	*
	* @param  string query     database query
	* 
	* @return mixed
	*/
	abstract public function dbQuery($query);
	
	/**
	 * Liefert eine Fehlermeldung der Datenbank zur�ck und gibt auch die Zeile
	 *   und die Datei zur�ck, die die Fehlermeldung ausgel�st hat.
	 */
	function dbError($msg, $query) {
    $debug = debug_backtrace();
    $str = '';
    if(isset($debug[3]['line'])) $str.=$debug[3]['line'].' ';
    return $str.$debug[2]['line'].') SQL-error: ('.$msg.'): '.$query;
	}





////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//                                                                            //
// Helpermethoden für die Formatierung von Datenfeldern                       //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
	/**
	* SQL: Bastelt einen SQL-Insert Statement Teil aus einem 1D-Assoz-Array
	*      Die ' bei den Values m�ssen selbst gesetzt werden!
	*
	* @param    array       Der Key gibt den Feldnamen an
	*                       Der Value gibt den Feldinhalt an
	*
	* @return          Liefert einen SQL String wie
	*                      "(feld1, feld5, feld3) VALUES('value1', 'value5'. 'value3')"
	*
	*/
	final static public function sqlInsert($array) {
		if(!is_array($array)) return $array;	  
	  
	  $values=$keys=array();
	  foreach($array as $key=>$val) {
      $values[]=self::escapeString($val);
      $keys[]=$key;
	  }
	  
	  return '('.implode(', ', $keys).') VALUES ('.implode(', ', $values).')';
	}
	
	
	
	/**
	* SQL: Bastelt einen SQL-Insert Statement Teil aus einem 1D-Assoz-Array
	*      Die ' bei den Values m�ssen selbst gesetzt werden!
	*
	* @param    array       Der Key gibt den Feldnamen an
	*                       Der Value gibt den Feldinhalt an
	*
	* @return          Liefert einen SQL String wie
	*                      "(feld1, feld5, feld3) VALUES('value1', 'value5'. 'value3')"
	*
	*/
	final static public function sqlInsertUnescaped($array) {
	  $values=$keys=array();
	  foreach($array as $key=>$val) {
      $values[]=$val;
      $keys[]=$key;
	  }
	  
	  return '('.implode(', ', $keys).') VALUES ('.implode(', ', $values).')';
	}
	
	
	
	/**
	* SQL: Bastelt einen SQL-UPDATE Statement Teil aus einem 1D-Assoz-Array
	*      Die ' bei den Values werden automatisch gesetzt
	*
	* @param    array       Der Key gibt den Feldnamen an
	*                       Der Value gibt den Feldinhalt an
	*
	* @return          Liefert einen SQL String wie
	*                      "feld1='value1', feld5='value5'"
	*
	*/
	final static public function sqlUpdate($array)	{
		if(!is_array($array)) return $array;	  

	  $pairs=array();
	  foreach($array as $key=>$val) {
      if(is_null($val)) {
      	$pairs[]=$key.'=NULL';
      }
      else {
      	$pairs[]=$key.'='.self::escapeString($val);
      } 
	  }
	  return implode(', ', $pairs);
	}
	


	/**
	* SQL: Bastelt einen SQL-UPDATE Statement Teil aus einem 1D-Assoz-Array
	*      Die ' bei den Values werden automatisch gesetzt
	*
	* @param    array       Der Key gibt den Feldnamen an
	*                       Der Value gibt den Feldinhalt an
	*
	* @return          Liefert einen SQL String wie
	*                      "feld1='value1', feld5='value5'"
	*
	*/
	final static public function sqlUpdateUnescaped($array)	{
	  $pairs=array();
	  foreach($array as $key=>$val) {
      if(is_null($val)) {
      	$pairs[]=$key.'=NULL';
      }
      else {
      	$pairs[]=$key.'='.$val;
      } 
	  }
	  return implode(', ', $pairs);
	}
	


  /**
	 * SQL: Bastelt einen SQL-WHERE Statement Teil aus einem 1D-Assoz-Array
	 *
	 * @param    array        Der Key gibt den Feldnamen an
	 *                        Der Value gibt den Feldinhalt an, muss vorher
	 *                          escaped werden
	 *
	 * @return  string
	 *
	 */
	final static public function sqlWhere($array) {
	  $pairs=array();
	  foreach($array as $key=>$val) {
      $pairs[]=self::escapeColumn($key).'='.self::escapeString($val);
	  }
	  return implode(' AND ', $pairs);
	}


	
  /**
	 * SQL: Bastelt einen SQL-WHERE Statement Teil aus einem 1D-Assoz-Array
	 *
	 * @param    array        Der Key gibt den Feldnamen an
	 *                        Der Value gibt den Feldinhalt an, muss vorher
	 *                          escaped werden
	 *
	 * @return  string
	 *
	 */
	final static public function sqlWhereUnescaped($array) {
	  $pairs=array();
	  foreach($array as $key=>$val) {
      $pairs[]=$key.'='.$val;
	  }
	  return implode(' AND ', $pairs);
	}


	
	/**
	 * Erstellt f�r ein und dasselbe Feld mehrere WHERE x=a AND x=b AND... 
	 *   Abfragen
	 * 
	 * @param  array   array      1D-Array mit den Werten
	 * @param  string  keyname    Name des Feldes
	 * @param  string  separator  Trenner zwischen den Feldern OR, AND
	 */
	final static public function sqlSameWhere($array, $key, $separator) {
		$empty=array();
		foreach($array as $val) {
			$empty[]=self::escapeColumn($key).'='.self::escapeString($val);
		}
		
		return implode(' '.$separator.' ', $empty);
	}






////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//                                                                            //
// Helpermethoden für die Formatierung von Datenfeldern                       //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
	/**
	 * Nimmt ein Array oder einen einfachen Wert und formatiert ihn zu einem
	 *   String
	 * 
	 * @param  mixed   array   string|array mit Werten
	 * 
	 * @return mixed
	 */
	public function escapeString($mixed) {
	  if(is_array($mixed)) {
	    foreach($mixed as $key=>$val) {
	      $mixed[$key]=self::escapeString($val);
	    }
	  }
	  else {
	    $mixed="'".str_replace("'", "\'", $mixed)."'";
//	    $mixed=str_replace('\n', "\n", $mixed);
	  }
	  return $mixed;
	}
	public function escapeNumber($number) {
		return intval($number);
	}
	public function escapeNull() {
		return 'NULL';
	}
	
	
	/**
	 * Escaped die Namen in den Keys des Array bzw. den einzelnen übergebene 
	 *   Wert.
	 * 
	 * @param  string|array   mixed   
	 * 
	 */
	public function escapeColumn($mixed) {
		if(is_array($mixed)) {
			$new = array();
			foreach($mixed as $key=>$v) {
				$new[self::escapeColumn($key)]=$v;
			}
			$mixed=$new;
		}
		else {
			$mixed = '`'.$mixed.'`';
		}
		return $mixed;
	}

	/**
	 * Formatiert aus einem OSUS_Date Objekt ein Datum
	 */
	public function escapeDate(OSUS_Date $date) {
		return $this->escapeString($date->to(DATE_MYSQLFORMAT));
	}


	/**
	 * Escaped die Werte des Arrays und gibt einen String zurück
	 * Geeignet, um die Namen der Spaltenlisten zu escapen 
	 * 
	 * @param  array   Numerisches Array mit Namen der Spalten als Keys   
	 * 
	 * @return string 
	 */
	public function escapeColumnNames($array) {
			$new = array();
			foreach($array as $v) {
				$new[]=$this->escapeColumn($v);
			}
		return implode(',', $new);
	}



////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//                                                                            //
// Transaktionen                                                              //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
	public function beginTransaction() {
		return $this->dbQuery('START TRANSACTION');
	}
	
	public function commitTransaction() {
		return $this->dbQuery('COMMIT');
	}
	
	public function rollbackTransaction() {
		return $this->dbQuery('ROLLBACK');
	}
}
?>