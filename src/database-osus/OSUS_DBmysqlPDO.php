<?php
/**
 * Datenbankzugriff auf mysql via PDO
 */
class OSUS_DBmysqlPDO extends OSUS_DBmysqlAbstract {

	/** dsn zum connecten via php_pdo */
	private $dsn=null;

	
	public function __construct($db) {
		$this->dsn = $db['type'].':host='.$db['host'].';port='.$db['port'].';dbname='.$db['db'];
		parent::__construct($db);
	}	


	public function beginTransaction() {
		$dbC = $this->getDbConnection();
		$dbC->beginTransaction();
	}
	
	public function commitTransaction() {
		$dbC = $this->getDbConnection();
		$dbC->commit();
	}
	
	public function rollbackTransaction() {
		$dbC = $this->getDbConnection();
		$dbC->rollback();
	}


	/**
	 * Liefert die Connection zur Datenbank zur�ck
	 */
	protected function getDbConnection() {
		if($this->dbConnection!=null) return $this->dbConnection;

		// Versuch der Verbindung zur DB 
		try {
		  $this->dbConnection = new PDO($this->dsn, 
		  		$this->host['user'], $this->host['pw']);
			$this->initCharset();
			return $this->dbConnection;
		} catch (PDOException $e) {
		   print "Error!: ".$e->getMessage() . "<br/>";
		   die();
		}		
	}



	/**
	 * Überschreibt Datensätze und nimmt an, dass sie meistens schon vorhanden 
	 *   sind
	 * 
	 * Rückgabewert: 
	 *   War Datensatz schon vorhanden und wurde überschrieben?
	 * 
	 * @param   string   table
	 * @param   mixed    set               Schon escapted Spalten und Werte
	 * @param   string   primaryKeyColumn  Name der Spalte des PrimaryKeys   
	 * 
	 * @throws Exception
	 * @return bool
	 */
	public function overwrite($table, $set, $primaryKeyColumn) {

	  if($connection = $this->getDbConnection()) {
	
			$withoutPrimary = $set;
			unset($withoutPrimary[$primaryKeyColumn]);
			$where = array($primaryKeyColumn=>$set[$primaryKeyColumn]);

			$command = 'UPDATE '.$table.' SET '.self::sqlUpdateUnescaped($withoutPrimary);
			$where=' WHERE '.self::sqlWhereUnescaped($where);
			$query = $command.' '.$where;

if(ECHO_ON) echo nl2br(microdate('H:i:s').' '.__CLASS__.'->overwrite(): '.$query.PHP_EOL);
			$affected = $connection->exec($query);
			$errorInfo=$connection->errorInfo(); 
			if(!isset($errorInfo[2])) {
				if(is_numeric($affected) and $affected!=0) return true;
		
				$this->set($table, $set);
			}
			else {
		  	throw new Exception($errorInfo[2].PHP_EOL.' - SQL_Statement: '.$query);
			}
	  }
		return false;
	}



	/**
	* Sende ein Query an die Datenbank
	* 
	* R�ckgabewert: 
	* Ein Array mit den Zeilen, bzw. ein leeres Array bei Misserfolg
	*
	* @param  string query     database query
	* 
	* @return array
	*/
	public function dbQuery($query) {

	  if($connection = $this->getDbConnection()) {

if(ECHO_ON) echo nl2br(microdate('H:i:s').' '.__CLASS__.'->dbQuery(): '.$query.PHP_EOL);
//if(strstr($query, '955')) debug(debug_backtrace());

		  $result = $connection->query($query);
		  $this->lastQuery=$query;
		  if($result!==false) {
				if(strpos($query, 'INSERT')===0) {
					return $this->lastInsertId();
				}		  
			  return $result->fetchAll(PDO::FETCH_ASSOC);
		  }
		  else {
		  	$err = $connection->errorInfo();
		  	throw new Exception($err[2].PHP_EOL.' - SQL_Statement: '.$query);
		  }	  	
	  }
	  
	  return array();
	}	



	/**
	 * Liefert die zuletzt ge�nderte ID eines UPDATEs zur�ck
	 *   Falls nicht m�glich, wird null zur�ckgegeben
	 */
	public function lastInsertId() {
	  if($connection = $this->getDbConnection()) {
	  	return $connection->lastInsertId();
	  }
	  return null;		
	}	

	/**
	 * Liefert die Anzahl der zuletzt ge�nderten Datens�tze
	 */
	public function affectedRows() {
	  if($connection = $this->getDbConnection()) {
	  	return $connection->affected_rows;
	  }
	  return null;		
	}	













////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//                                                                            //
// Spezialzusatzfunktionen                                                    //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
	/**
	 * Eine lokale Tabelle in eine entfernte DB eintragen
	 */
	public function dbSyncTables($dbHome, $dbFern, $tablenames) {
		db_open($dbHome);
		db_open($dbFern);
	
die('Funktion muss noch an diese DB Objekt angepa�t werden');
		foreach($tablenames as $tablename) {
			if(empty($tablename)) continue;
	
			$rows = db_get($dbHome, $tablename, 'count(*)');
			if($rows===false or count($rows)==0) continue;
			$count=0;
			$maxcount=$rows[0][0];
			$step=5000;
			echo 'Synchronisiere Tabelle '.$tablename.': ';
			$sql = 'TRUNCATE '.$tablename;
			$req='';
			$req = $this->dbQuery($dbFern, $sql);
	
			// Zeilen in Zieldatenbank eintragen
			while(1) {
				$rows = db_get($dbHome, $tablename, '*', '', 'LIMIT '.$count.', '.$step);
				progress('sync', 0, $maxcount);
				foreach($rows as $row) {
					$data = array_unset_entry($row, 1);
					db_set($dbFern, $tablename, $data);
	
					$count++;
					progress('sync', $count);
				}
				// Schleife verlassen, weil keine Datens�tze mehr zu lesen sind
				if(count($rows)<$step) break;
			}
		}
	}
}
?>