<?php
class OsusException extends ErrorException {
    private $description = "";

    static public function customErrorHandler($errno, $errstr, $errfile = "", $errline = 0, $errcontext = array()) {
        throw new OsusException($errstr, $errno, 1, $errfile, $errline);
    }
    
    public function getDescription() {
        return $this->description;
    }
    
    public function setDescription($description) {
        return $this->description = $description;
    }
    
    public function toJson() {
        return json_encode(
            array(
                "code" => $this->getCode(),
                "message" => $this->getMessage(),
                "description" => $this->getDescription()
            )
        );
    }
}
