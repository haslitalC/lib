<?php
/**
 * Registry-Pattern
 *
 * Created on 23.03.2008
 * 
 * @since v0.1 
 * @author Thorsten Koch
 *
 */
class OSUS_Registry {

	/** Registry Speicher */
	static $registry=array();

	
	
	static public function getInstance() {
		return self;
	}
	
	
	
	/**
	 * Liefert einen Wert aus der Registry zurück
	 */
	static public function get($name) {

		if(!isset(self::$registry[$name])) {
			global $$name;
			self::set($name, $$name);
		}

		return self::$registry[$name];
	}
	
	
	/**
	 * Setzt einen Wert in die Registry
	 */
	static public function set($name, $value) {
		self::$registry[$name]=$value;
	}
}
