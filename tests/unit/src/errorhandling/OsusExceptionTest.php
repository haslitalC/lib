<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "index.php";

class OsusExceptionTest extends PHPUnit_Framework_TestCase {
  
  
    /**
     * @expectedException ErrorException
     * @expectedExceptionMessage das ist die message
     * @expectedExceptionCode 1234
     */
    public function testCustomErrorHandler() {
        $actual = new OsusException();
        $actual->customErrorHandler(1234, "das ist die message");
    }

    
    
    public function testDescription() {
        $actual = new OsusException();
        $this->assertEmpty($actual->getDescription());
        
        $actual->setDescription("blablub");
        $this->assertEquals("blablub", $actual->getDescription());
    }


    
    public function testExceptionThrownDescription() {
        try {
            throw new OsusException("huhu", 55); 
        } catch (Exception $actual) {
            $this->assertEquals(55, $actual->getCode());
            $this->assertEquals("huhu", $actual->getMessage());
            $this->assertEmpty($actual->getDescription());
        }

        try {
            $exception =  new OsusException("huhu", 55); 
            $exception->setDescription("desc irgendwas");
            throw $exception;
        } catch (Exception $actual) {
            $this->assertEquals(55, $actual->getCode());
            $this->assertEquals("huhu", $actual->getMessage());
            $this->assertEquals("desc irgendwas", $actual->getDescription());
        }
    }


    
    public function testToJson() {
        try {
            throw new OsusException("huhu", 55); 
        } catch (Exception $actual) {
            $this->assertEquals('{"code":55,"message":"huhu","description":""}', $actual->toJson());
        }

        try {
            $exception =  new OsusException("huhu", 55); 
            $exception->setDescription("desc irgendwas");
            throw $exception;
        } catch (Exception $actual) {
            $this->assertEquals(55, $actual->getCode());
            $this->assertEquals("huhu", $actual->getMessage());
            $this->assertEquals("desc irgendwas", $actual->getDescription());
        }
    }
}
