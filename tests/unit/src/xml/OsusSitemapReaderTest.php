<?php

class OsusSitemapReaderTest extends PHPUnit_Framework_TestCase {
  
  
  public function testNotFoundSitemapUrl() {
    $url = "https://test-your-google-pagespeed.com/testdata/error404.php";
    try {
      $ott = new OsusSitemapReader();
      $ott->readLocationsFromFile($url);
      $this->fail("Exception not thrown");
    } catch (Exception $e) {
      $this->assertContains("simplexml_load_file(" . $url . "): failed to open stream: HTTP request failed! HTTP/1.1 404 Not Found", $e->getMessage());
    }
  }
  
  
  
  public function testInvalidSitemapXml() {
    try {
      $ott = new OsusSitemapReader();
      $ott->readLocationsFromFile("https://test-your-google-pagespeed.com/testdata/sitemapInvalid.xml");
      $this->fail("Exception not thrown");
    }
    catch (Exception $e) {
      $this->assertContains("Opening and ending tag mismatch: urlset line 2 and urlse", $e->getMessage());
    }
  }
  
  
  
  public function testReadLocationsFromString() {
    $string = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . "files" .  DIRECTORY_SEPARATOR . "sitemap.xml");
    
    $ott = new OsusSitemapReader();
    $actual = $ott->readLocationsFromString($string);
    
    $this->assertFalse($actual[0] instanceof SimpleXmlElement);
    
    $this->assertEquals("https://designinmetall.de/", $actual[0]);
    $this->assertEquals("https://designinmetall.de/news/", $actual[1]);
    $this->assertEquals("https://designinmetall.de/news/2016/02/14/neue-website/", $actual[2]);
    $this->assertEquals("https://designinmetall.de/news/2016/02/16/neue-termine-fuer-maerz16/", $actual[3]);
    
    $this->assertCount(30, $actual);
  }
  
  
  
  public function testReadLocationsFromStringInvalid() {
    $string = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . "files" .  DIRECTORY_SEPARATOR . "sitemapInvalid.xml");
    
    try {
      $ott = new OsusSitemapReader();
      $ott->readLocationsFromString($string);
      
    } catch (Exception $e) {
      $this->assertContains("Extra content at the end of the document", $e->getMessage());
    }
  }
  
  
  
  public function testReadLocationsFromFileWithFile() {
    $ott = new OsusSitemapReader();
    $actual = $ott->readLocationsFromFile(__DIR__ . DIRECTORY_SEPARATOR . "files" .  DIRECTORY_SEPARATOR . "sitemap.xml");
    
    $this->assertEquals("https://designinmetall.de/", $actual[0]);
    $this->assertEquals("https://designinmetall.de/news/", $actual[1]);
    $this->assertEquals("https://designinmetall.de/news/2016/02/14/neue-website/", $actual[2]);
    $this->assertEquals("https://designinmetall.de/news/2016/02/16/neue-termine-fuer-maerz16/", $actual[3]);
    
    $this->assertCount(30, $actual);
  }
  
  
  
  public function testReadLocationsFromFileWithFileNotFound() {
    $ott = new OsusSitemapReader();
    try {
      $xmlFilename = __DIR__ . DIRECTORY_SEPARATOR . "files" .  DIRECTORY_SEPARATOR . "sadfgf.xml";
      $ott->readLocationsFromFile($xmlFilename);
      $this->fail("Exception not thrown");
    } catch (Exception $e) {
      $this->assertEquals("failed to load external entity \"" . $xmlFilename . "\"" . PHP_EOL, $e->getMessage());
    }
  }
  
  
  
  /**
   * @expectedException Exception
   * @expectedExceptiomMessage XML declaration allowed only at the start of the document
   */
  public function testReadLocationsFromFileWithFileInvalid() {
    $ott = new OsusSitemapReader();
    $ott->readLocationsFromFile(__DIR__ . DIRECTORY_SEPARATOR . "files" .  DIRECTORY_SEPARATOR . "sitemapInvalid.xml");
    $this->fail("Exception not thrown");
  }
  
  
  
  public function testReadLocationsFromFileWithUrl() {
    $ott = new OsusSitemapReader();
    $actual = $ott->readLocationsFromFile("https://test-your-google-pagespeed.com/testdata/sitemap.xml");
    
    $this->assertEquals("https://test-your-google-pagespeed.com/examples/noErrors.html", $actual[0]);
    $this->assertEquals("https://test-your-google-pagespeed.com/examples/avoid-redirects/", $actual[1]);
    $this->assertEquals("https://test-your-google-pagespeed.com/examples/avoid-render-blocking-resources", $actual[2]);
    $this->assertEquals("https://test-your-google-pagespeed.com/examples/reduce-server-respond-time", $actual[10]);
    
    $this->assertCount(11, $actual);
  }
}
