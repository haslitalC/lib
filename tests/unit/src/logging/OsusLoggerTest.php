<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "index.php";

class OsusLoggerTest extends PHPUnit_Framework_TestCase {
  
  private $logFileName;
  
  public function testInfo() {
    $actual = new OsusLoggerMock();
    
    $message = "huhu";
    $actual->info($message);

    $this->assertContains($message, $actual->getLastMessage());
    $this->assertContains(date("Ymd H"), $actual->getLastMessage());
  }

  public function testWarning() {
    $actual = new OsusLoggerMock();
    
    $message = "huhu";
    $actual->warning($message);

    $this->assertContains("Warning: " . $message, $actual->getLastMessage());
    $this->assertContains(date("Ymd H"), $actual->getLastMessage());
  }

  public function testError() {
    $actual = new OsusLoggerMock();
    
    $message = "huhu";
    $actual->error($message);

    $this->assertContains("ERROR: " . $message, $actual->getLastMessage());
    $this->assertContains(date("Ymd H"), $actual->getLastMessage());
  }

  public function testMany() {
    $actual = new OsusLoggerMock();
    
    $message = "huhu";
    $actual->warning($message);
    $actual->error($message);
    
    $this->assertCount(2, $actual->getMessages());
  }
}
