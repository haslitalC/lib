<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "index.php";

class OsusFileLoggerTest extends PHPUnit_Framework_TestCase {
  
  private $logFileName;
  
  public function __construct() {
    $this->logFileName = __DIR__ . DIRECTORY_SEPARATOR . "test.log";
    file_put_contents($this->logFileName, "");
  }
  
  
  public function testInfo() {
    $file = new OsusSplFileObjectMock($this->logFileName);
    $actual = new OsusFileLogger($file);
    
    $message = "huhu";
    $actual->info($message);

    $this->assertContains($message . PHP_EOL, $file->getLastWritten());
    $this->assertContains(date("Ymd H"), $file->getLastWritten());
  }

  public function testWarning() {
    $file = new OsusSplFileObjectMock($this->logFileName);
    $actual = new OsusFileLogger($file);
    
    $message = "huhu";
    $actual->warning($message);

    $this->assertContains("Warning: " . $message . PHP_EOL, $file->getLastWritten());
    $this->assertContains(date("Ymd H"), $file->getLastWritten());
  }

  public function testError() {
    $file = new OsusSplFileObjectMock($this->logFileName);
    $actual = new OsusFileLogger($file);
    
    $message = "huhu";
    $actual->error($message);

    $this->assertContains("ERROR: " . $message . PHP_EOL, $file->getLastWritten());
    $this->assertContains(date("Ymd H"), $file->getLastWritten());
  }
  
  public function testRealWrite() {
    $file = new SplFileObject($this->logFileName);
    $actual = new OsusFileLogger($file);
    
    $message = "huhu";
    $actual->error($message);

    $this->assertContains("ERROR: huhu" . PHP_EOL, file_get_contents($file->getPathname()));
    $message = "hu2hu";
    $actual->error($message);

    $this->assertContains("ERROR: huhu" . PHP_EOL, file_get_contents($file->getPathname()));
    $this->assertContains("ERROR: hu2hu" . PHP_EOL, file_get_contents($file->getPathname()));
  }
  
  public function testSecondWrite() {
    $file = new SplFileObject($this->logFileName);
    $actual = new OsusFileLogger($file);
    
    $message = "second";
    $actual->error($message);

    $this->assertContains("ERROR: huhu" . PHP_EOL, file_get_contents($file->getPathname()));
    $this->assertContains("ERROR: hu2hu" . PHP_EOL, file_get_contents($file->getPathname()));
    $this->assertContains("ERROR: second" . PHP_EOL, file_get_contents($file->getPathname()));
    
    unset($file);
    unlink($this->logFileName);
  }
}
