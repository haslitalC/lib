<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "index.php";

// use PHPUnit\Framework\TestCase;


class OsusConfigurationTest extends PHPUnit_Framework_TestCase {
    
  private $testPath = "files";
  private $testManyFilesPath = "many-files";
  private $testWrongFormatsPath = "wrong-format";
  
  private $actual = null;
  
  public function __construct() {
    $this->testPath =             __DIR__ . DIRECTORY_SEPARATOR . $this->testPath . DIRECTORY_SEPARATOR;
    $this->testManyFilesPath =    __DIR__ . DIRECTORY_SEPARATOR . $this->testManyFilesPath . DIRECTORY_SEPARATOR;
    $this->testWrongFormatsPath = __DIR__ . DIRECTORY_SEPARATOR . $this->testWrongFormatsPath . DIRECTORY_SEPARATOR;
  }
  
  
  
  /**
   * @expectedException Exception
   * @expectedExceptionMessage Singleton not created yet, $pathToConfigFiles parameter is mandatory
   */
  public function testSingletonException() {
    $ott = OsusConfiguration::getInstance();
  }
  
  
  
  public function testConfigFromOneFile() {
    $actual = $this->createObjectToTest();
    
    $this->assertEquals("localhost", $actual->get("database.host"));
    $this->assertEquals("root", $actual->get("database.user"));
    $this->assertEquals("", $actual->get("database.password"));
    $this->assertEquals("mysql", $actual->get("database.type"));
    $this->assertEquals("pdo", $actual->get("database.driver"));
    $this->assertEquals("scalarValue", $actual->get("scalar"));
    $this->assertCount(1, $actual->readFiles); // its loaded twice, because of the createObjectToTest()
  }
  

  
  public function testConfigFromManyFile() {
    $actual = $this->createObjectToTest("manyFiles");
    
    $this->assertEquals("pagespeed_test", $actual->get("database.name"));
    $this->assertEquals("root", $actual->get("database.user"));
    $this->assertEquals("scalarValue", $actual->get("scalar"));
    $this->assertEquals("AIzaSyDGA9m5IwtfT-XjFAfm05ZX_dtVgiGYTgE", $actual->get("googleapi.api_key"));
    
    $this->assertCount(4, $actual->readFiles);
  }
  

  
  /**
   * @expectedException Exception
   * @expectedExceptionMessage invalid file, cannot read file with extension "txt"
   */
  public function testConfigWrongFormats() {
    $actual = $this->createObjectToTest("wrongFormat");
  }
  

  
  private function createObjectToTest($type = "standard") {
    
    switch($type) {
        case "standard":
          $path = $this->testPath;
        break;
        case "manyFiles":
          $path = $this->testManyFilesPath;
        break;
        case "wrongFormat":
          $path = $this->testWrongFormatsPath;
        break;
        default:
          throw new Exception("unknown type " . $type);
    }

    // to load other pathes, because getInstance() returns a singleton and $path will not overwritten!      
    $ott = OsusConfiguration::getInstance($path);
    //$ott->loadConfigs($path);     

    return $ott;
  }
    
}